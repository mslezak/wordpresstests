﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using WordpressTests.Extensions;

namespace WordpressTests.TestSettings
{
    public class WebDriverFactory
    {
        public RemoteWebDriver ReturnWebDriver(Browsers browsers)
        {
            switch (browsers)
            {
                case Browsers.FirefoxLocal:
                    return new FirefoxDriver();
                case Browsers.FirefoxRemote:
                    var firefoxOptions = new FirefoxOptions();
                    firefoxOptions.AddAdditionalCapability("enableVNC", true, true);
                    firefoxOptions.AddAdditionalCapability("enableVideo", true, true);
                    return new RemoteWebDriver(new Uri("http://vps591118.ovh.net:4444/wd/hub"), firefoxOptions);
                case Browsers.ChromeLocal:
                    return new ChromeDriver();
                case Browsers.ChromeRemote:
                    var options = new ChromeOptions();
                    options.BrowserVersion = "69.0";
                    options.AddAdditionalCapability("enableVNC",true,true);
                    options.AddAdditionalCapability("enableVideo",true,true);
                    return new RemoteWebDriver(new Uri("http://vps591118.ovh.net:4444/wd/hub"), options);
                default:
                    throw new ArgumentOutOfRangeException(nameof(browsers), browsers, null);
            }
        }
    }
}